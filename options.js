function onChange (style) {
  return (example, prop, val) => {
    browser.storage.local.get({
      mainStyle: utils.defaultStyle,
      visitedStyle: null
    }).then(res => {
      const stl = (!res.visitedStyle || style === 'mainStyle')
        ? res.mainStyle
        : res.visitedStyle

      stl[prop] = val
      browser.storage.local.set({ [style]: stl })

      const [ sProp, sVal ] = utils.confToStyle(prop, val)
      example.style[sProp] = sVal
    })
  }
}

function restoreOptions () {
  fetch(browser.extension.getURL('default-domains.json'))
    .then(res => res.json())
    .then(res => {
      const defaultDomains = res.domains

      browser.storage.local.get({
        mainStyle: utils.defaultStyle,
        visitedStyle: null,
        useVisitedStyle: false,
        domains: defaultDomains
      }).then(result => {
        const styleOptions = document.getElementById('style-options')
        const visitedForm = utils.buildStyleForm('Visited links', onChange('visitedStyle'), result.visitedStyle || result.mainStyle)
        styleOptions.appendChild(utils.h('label', {}, [
          utils.t('Use another style for already visited links'),
          utils.h('input', { type: 'checkbox', checked: result.useVisitedStyle }, [], {
            change: (_, checked) => {
              if (checked) {
                visitedForm.style.display = 'block'
                browser.storage.local.set({ useVisitedStyle: true })
              } else {
                visitedForm.style.display = 'none'
                browser.storage.local.set({ useVisitedStyle: false })
              }
            }
          })
        ]))

        styleOptions.appendChild(utils.buildStyleForm('Normal style', onChange('mainStyle'), result.mainStyle))

        visitedForm.style.display = result.useVisitedStyle ? 'block' : 'none'
        styleOptions.appendChild(visitedForm)

        result.domains.forEach(createDomainLi)
        document.getElementById('add-domain').addEventListener('submit', evt => {
          evt.preventDefault()
          const domain = document.getElementById('domain').value.replace(/(https?:\/\/)?(www.)?/, '').split('/')[0]
          createDomainLi(domain, result.domains.length, result.domains)
          result.domains.push(domain)
          browser.storage.local.set({ domains: result.domains })
          document.getElementById('domain').value = ''
        })
      }).catch(console.error)

      document.getElementById('import').addEventListener('click', handleImport())

      document.getElementById('export').addEventListener('click', handleExport(defaultDomains))
    })
}

function handleImport () {
  return evt => {
    const fileInput = utils.h('input', {
      style: {
        display: 'none'
      },
      type: 'file',
      accept: '.json',
      acceptCharset: 'utf-8'
    },
    [],
    {
      change: evt => {
        if (fileInput.value !== fileInput.initialValue) {
          document.body.style.cursor = 'wait'

          const file = fileInput.files[0]
          const fReader = new FileReader()
          fReader.addEventListener('loadend', event => {
            fileInput.remove()
            const importedDomains = JSON.parse(event.target.result).domains
            browser.storage.local.get({ domains: [] }).then(res => {
              const newDomains = importedDomains.filter(d => !res.domains.includes(d))
              const domains = newDomains.concat(res.domains)

              const list = document.getElementById('domain-list')
              while (list.firstChild) {
                list.removeChild(list.firstChild)
              }

              domains.forEach(createDomainLi)
              document.body.style.cursor = ''
            })
          })
          fReader.readAsText(file, 'utf-8')
        }
      }
    })
    fileInput.initialValue = fileInput.value
    document.body.appendChild(fileInput)
    fileInput.click()
  }
}

function handleExport (defaultDomains) {
  return evt => {
    browser.storage.local.get({ domains: defaultDomains }).then(res => {
      const data = JSON.stringify(res)
      const blob = new Blob([ data ], { type: 'octet/stream' })
      const objectURL = URL.createObjectURL(blob)
      browser.downloads.download({
        url: objectURL,
        filename: 'gafalink-domains.json'
      })
    })
  }
}

function createDomainLi (domain, i, domains) {
  const parent = document.getElementById('domain-list')
  const li = document.createElement('li')

  const label = document.createElement('p')
  label.appendChild(document.createTextNode(domain))
  const delBt = document.createElement('a')
  delBt.href = '#'
  delBt.appendChild(document.createTextNode('Delete'))
  delBt.addEventListener('click', evt => {
    evt.preventDefault()
    parent.removeChild(li)
    browser.storage.local.set({ domains: domains.splice(i, 1) })
  })

  li.appendChild(label)
  li.appendChild(delBt)

  parent.appendChild(li)
}

document.addEventListener('DOMContentLoaded', restoreOptions)
