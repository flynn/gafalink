browser.runtime.onMessage.addListener(msg => {
  return new Promise(resolve => {
    browser.history.search({ text: '', startTime: 0 }).then(history => {
      console.log(history)
      resolve({ history: history })
    })
  })
})
