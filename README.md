# GAFALink

This small extension will highlight links to proprietary services online. Then, it will be easier for you
to avoid visiting them, and remember to prefer libre alternatives.

Thanks to **valvin**, **flynn**, **Florestan**, **mmu_man**, **drulac**, **gildas** and
who contributed to the list of domains to highlight.

## Contributing

- Fork this repo
- Clone your fork on your computer
- `npm i --dev`
- Start working on a new branch
- Prefer small commits
- Once your done, push to your fork
- Open a Merge Request

To test your changes, I would advice you to use the `web-ext` tool. To install it:

```
npm i -g web-ext
```

Then, to run your extension

```
web-ext run
```
