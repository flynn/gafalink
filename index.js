fetch(browser.extension.getURL('default-domains.json'))
  .then(res => res.json())
  .then(res => {
    const gafamHosts = res.domains

    browser.storage.local.get({
      mainStyle: utils.defaultStyle,
      visitedStyle: null,
      useVisitedStyle: false,
      domains: gafamHosts
    }).then(conf => {
      browser.runtime.sendMessage({ msg: 'please, give me the history' }).then(msg => {
        console.log(msg)
        if (conf.useVisitedStyle && conf.visitedStyle) {
          color(l => !msg.history.some(h => h.url === l.href), conf.mainStyle, conf.domains)
          color(l => msg.history.some(h => h.url === l.href), conf.visitedStyle, conf.domains)
        } else {
          color(_ => true, conf.mainStyle, conf.domains)
        }
      })
    })

    function color (filter, style, domains) {
      const links = Array.prototype.slice.call(document.querySelectorAll('a')).filter(filter)
      console.log(links)
      links.forEach(highlight(style, domains))

      const observer = new MutationObserver(muts => {
        for (const mut of muts) {
          mut.addedNodes.forEach(node => {
            highlight(style, domains)(node)
            try {
              Array.prototype.slice.call(node.querySelectorAll('a')).filter(filter).forEach(highlight(style, domains))
            } catch (e) {
              console.error(node, e)
            }
          })
        }
      })

      observer.observe(document.body, {
        attributes: true,
        childList: true,
        subtree: true,
        attributeFilter: [ 'href' ]
      })
    }

    function highlight (conf, domains) {
      return (link, i, ls) => {
        // console.log(ls)
        if (link.hostname && link.hostname !== window.location.hostname) {
          if (domains.some(h => link.hostname.endsWith(h))) {
            utils.styleLink(link, conf)
          }
        }
      }
    }
  })
